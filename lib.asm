global string_equals
global read_word
global print_string
global read_char
global string_length
global print_error
section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    xor rax, rax
    mov rax, 60
    xor rdx, rdx
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
	cmp byte[rdi + rax], 0
	je .end
	inc rax
	jmp .loop
.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    	call string_length
	mov rdx, rax
	mov rsi, rdi
	mov rdi, 1
	mov rax, 1
	syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rdi, 1
    mov rdx, 1
    mov rax, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    mov rdi, 0xA
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
    mov rax, rdi
    mov r10, 0x0A
    push 0x00
    cmp rax, 0
    jz .zero
.loop:
    xor rdx, rdx
    div r10
    add rdx, '0'
    push rdx
    cmp rax, r10
    jae .loop
    cmp rax, 0
    jz .loop2
.zero:
    add rax, '0'
    push rax
.loop2:
    cmp r11, 1
    jne .continue
    mov rdi, 0x2D
    call print_char
    xor r11, r11
.continue:
    pop rdi
    cmp rdi, 0x00
    je .end
    call print_char
    jmp .continue
.end:
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    mov rax, rdi
    push rax
    cmp rax, 0
    jl .minus
    pop rax
    mov rdi, rax
    call print_uint
    ret
.minus:
    pop rax
    neg rax
    mov r11, 1
    mov rdi, rax
    call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
.loop:
    xor r8, r8
    mov r8b, byte[rdi]
    cmp r8b, byte[rsi]
    jne .false
    cmp byte[rdi], 0x00
    je .true
    inc rdi
    inc rsi
    jmp .loop
.false:
    mov rax, 0
    ret
.true:
    mov rax, 1
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    mov rax, 0
    mov rdi, 0
    mov rdx, 1
    push 0
    mov rsi, rsp
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    mov r9, rdi;адрес буфера
    mov r10, rsi ;длина буфера
    push r9
    xor r8, r8
    mov r8, 0
.first_loop:
    call read_char
    test rax, rax
    jz .exit
    cmp rax, 0x20
    jle .first_loop
.second_loop:
    cmp r8, r10
    je .exit_fail
    mov [r9], rax
    inc r9
    inc r8
    call read_char
    cmp rax, 32
    jge .second_loop
.exit:
    mov byte[r9], 0x00
    pop r9
    mov rax, r9
    mov rdx, r8
    ret
.exit_fail:
    pop r9
    mov byte[r9], 0x00
    mov rax, 0
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    mov r10, 10
    xor r9, r9
    cmp r8, 1
    jne .loop
    mov r11, 1
.loop:
    cmp byte[rdi+r9], '0'
    jb .fail
    cmp byte[rdi+ r9], '9'
    ja .fail
    xor rdx, rdx
    xor r8, r8
    mul r10
    mov r8b, byte[rdi + r9]
    sub r8b, '0'
    add rax, r8
    inc r9
    jmp .loop
.fail:
    cmp r11, 1
    jne .end
    inc r9
.end:
    mov rdx, r9
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor r9, r9
.loop:
    cmp byte[rdi+r9], 0x20
    jle .loop
    cmp byte[rdi+r9], 0x2D
    jne .number
    inc rdi
    cmp byte[rdi+r9], 0x20
    jle .fail
    mov r8, 1
    call parse_uint
.number:
    call parse_uint
    ret 
.fail:
    mov rdx, 0
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    call string_length
    cmp rax, rdx
    jbe .copy
    mov rax, 0
    ret
.copy:
    push rax
    xor r11, r11
.loop:
    mov al, byte[rdi + r11]
    mov byte[rsi + r11], al
    inc r11
    cmp al, 0
    jnz .loop
    pop rax
    ret

;Принимает указатель на строку
print_error:
	xor rax, rax
	call string_length
	mov rdx, rax
	mov rsi, rdi
	mov rax, 1
	mov rdi, 2
	syscall
	ret
