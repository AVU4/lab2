lib.o: lib.asm
	nasm -g lib.asm -felf64 -o lib.o

dict.o: dict.asm lib.asm
	nasm -g dict.asm -felf64 -o dict.o 

main.o: main.asm colon.inc words.inc
	nasm -g main.asm -felf64 -o main.o

main: main.o dict.o lib.o
	ld -o main main.o dict.o lib.o
