extern read_word
extern find_word
extern string_length
extern print_string
extern print_error
global _start

section .data
%include "colon.inc"
%include "words.inc"
	input_message : db 'The name is too big', 0
	message: db 'This word was not found', 0
	buffer: times 256 db 0 

section .text

_start:
	mov rdi, buffer
	mov rsi, 256
	call read_word
	cmp rax, 0
	je .fail
	mov rdi, pointer
	call find_word
	cmp rax, 0
	jz .not_found
	lea rdi, [rax + 8]
	call string_length
	add rdi, rax
	inc rdi
	call print_string
	jmp .end
.fail:
	mov rdi, input_message
	call print_error
	jmp .end
.not_found:
	mov rdi, message
	call print_error
.end:
	mov rax, 60
	xor rdi, rdi
	syscall
	
	
