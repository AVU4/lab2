extern string_equals
global find_word

section .text

;rsi - string
;rdi - pointer

find_word:
	mov rsi, rax
	xor rax, rax
.loop:
	cmp rdi, 0
	jz .not_found
	push rdi
	push rsi
	lea rdi, [rdi + 8]
	call string_equals
	pop rsi
	pop rdi
	cmp rax, 0
	jnz .found
	mov rdi, [rdi]
	jmp .loop
.not_found:
	mov rax, 0
	ret
.found:
	mov rax, rdi
	ret	
